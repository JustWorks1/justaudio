#include <QApplication>
#include <QMediaPlayer>
#include <QSystemTrayIcon>
#include <QObject>
#include <QFileSystemWatcher>
#include <QSharedMemory>
#include <QDir>
#include <QFile>
#include <QIcon>

#include "aud_file.h"
#include "core.h"

//    This file is part of JustAudio.

//    JustAudio is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    JustAudio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with BashWire under the GPL.txt file. If not, see
//    <http://www.gnu.org/licenses/>.

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setApplicationName("JustAudio");
    app.setApplicationVersion("1.0.0");
    app.setQuitOnLastWindowClosed(false);

    QString path = QDir::homePath() + QDir::separator();

#ifndef Q_OS_WIN32

    path.append(QString(".config") + QDir::separator());

#else

    path.append(QString("AppData") + QDir::separator());
    path.append(QString("Local") + QDir::separator());

#endif

    QDir dir;

    path.append(QApplication::applicationName());
    dir.mkpath(path);

    QDir::setCurrent(path);

    if (!QFile::exists(PROCESS_FILE))
    {
        // QFileSystemWatcher will not work if the PROCESS_FILE file does not exists so
        // a base file need to be created to insure it connects to a file.

        QFile file(PROCESS_FILE, &app);

        file.open(QFile::WriteOnly | QFile::Truncate);
        file.write(" ");
        file.close();
    }

    // using a shared memory segment to determine if an instance of the app is running.
    // QSharedMemory::create() returns false if that is the case and true if not.
    // if an instance of the app is already runnning, the requested file path to playback
    // is written to PROCESS_FILE, that modification to the file is picked up by
    // QFileSystemWatcher and then read by the currently running instance and starts
    // playback of the file path written to PROCESS_FILE.

    QSharedMemory sharedMem(app.applicationName(), &app);

    if (sharedMem.create(SEGMENT_SIZE))
    {
        QFileSystemWatcher processFileWatcher(&app);
        QSystemTrayIcon    trayIcon(&app);
        QMediaPlayer       player(&app);
        Core               core(&app);

        core.setFileWatcher(&processFileWatcher);
        core.setPlayer(&player);
        core.setTrayIcon(&trayIcon);
        processFileWatcher.addPath(PROCESS_FILE);
        trayIcon.setToolTip(app.applicationName() + " " + app.applicationVersion());
        trayIcon.setIcon(QIcon(":/png/logo"));
        trayIcon.show();

        if (app.arguments().size() > 1)
        {
            core.play(app.arguments()[1]);
        }

        return app.exec();
    }
    else
    {
        if (app.arguments().size() > 1)
        {
            QFile file(PROCESS_FILE, &app);

            file.open(QFile::WriteOnly | QFile::Truncate);
            file.write(app.arguments()[1].toUtf8());
            file.close();
        }

        return 0;
    }
}
