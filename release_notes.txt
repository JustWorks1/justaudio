5/6/17 - 1.0.0

- Made some major changes to the JustAudio project.
- The majority of user interaction is now in the system tray icon. There's no longer a traditional user interface window.
- Added a proper application version number, GPL and source code comments.
- The application will now allow only one instance and will play the file requested in the arguments in the currently running instance.
- Added mute control.
- Settings are now saved in csv file format instead of idm.
- Several bug fixes.
- Seeker removed for now. (might but it back in the future)