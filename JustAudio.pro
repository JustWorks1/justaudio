#-------------------------------------------------
#
# Project created by QtCreator 2016-10-01T14:15:33
#
#    This file is part of JustAudio.
#
#    JustAudio is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    JustAudio is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BashWire under the GPL.txt file. If not, see
#    <http://www.gnu.org/licenses/>.
#
#-------------------------------------------------

QT += core gui
QT += multimedia
QT += svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET   = JustAudio
TEMPLATE = app

SOURCES += main.cpp \
    aud_file.cpp \
    core.cpp

HEADERS += \
    aud_file.h \
    core.h

RESOURCES += \
    res.qrc

RC_FILE = logo.rc
