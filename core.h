#ifndef CORE_H
#define CORE_H

//    This file is part of JustAudio.

//    JustAudio is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    JustAudio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with BashWire under the GPL.txt file. If not, see
//    <http://www.gnu.org/licenses/>.

#include <QAction>
#include <QObject>
#include <QMediaContent>
#include <QIODevice>
#include <QMessageBox>
#include <QStringList>
#include <QFileInfo>
#include <QList>
#include <QFileDialog>
#include <QAction>
#include <QIcon>
#include <QSystemTrayIcon>
#include <QMediaPlayer>
#include <QFileSystemWatcher>
#include <QMenu>
#include <QApplication>
#include <QCheckBox>
#include <QWidgetAction>
#include <QSlider>
#include <QHBoxLayout>
#include <QLabel>
#include <QToolButton>
#include <QDialog>
#include <QVBoxLayout>
#include <QPlainTextEdit>
#include <QTextOption>
#include <QDesktopWidget>
#include <QRect>

#include "aud_file.h"

#define SEGMENT_SIZE 10
#define PROCESS_FILE "process.txt"
#define CONFIG_FILE  "conf.csv"

class Core : public QObject
{
    Q_OBJECT

private:

    enum ConfValues
    {
        VOLUME,
        PLAY_DIR_CHECK,
        MUTE
    };

    AudFile         *audFile;
    QAction         *playAction;
    QAction         *pauseAction;
    QAction         *stopAction;
    QAction         *nextAction;
    QAction         *prevAction;
    QCheckBox       *nextCheck;
    QCheckBox       *muteCheck;
    QSlider         *volumeSlider;
    QLabel          *currentFile;
    QSystemTrayIcon *sysTray;
    int              mediaState;

    void        fileDir(char direction);
    QStringList audExtensions();

private slots:

    void trayClicked(QSystemTrayIcon::ActivationReason reason);
    void stateChanged(QMediaPlayer::State state);
    void processfileChanged(const QString &path);
    void nextFile();
    void prevFile();
    void playBackFinished();
    void openDialog();
    void volumeUp();
    void volumeDown();
    void showLicense();

public:

    void setTrayIcon(QSystemTrayIcon *tray);
    void setPlayer(QMediaPlayer *player);
    void setFileWatcher(QFileSystemWatcher *watcher);
    void play(const QString &path);

    Core(QObject *parent = 0);
    ~Core();

signals:

    void start();
    void pause();
    void stop();
    void setMedia(const QMediaContent &media, QIODevice *stream);
    void setVolume(int value);
    void muteState(bool state);
};

#endif // CORE_H
