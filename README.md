# What is JustAudio? #

JustAudio is a simplified audio player designed to be used only for playing music files on the local filesystem and that's it. it doesn't read ID3 data, manage your music collection or stream anything. this app is good for users that already have their music files organized to their liking in the local file system and doesn't want the extra fluff that must media players currently have today. 

### How do I get set up? ###

This application was built using the [QT](https://www.qt.io/) API. To compile or modify this application, just download and install the QT API, there's usually no need for additional APIs or libraries (depends on the compiler you chose for QT). Versions 4.8 and up are recommended.

### Who do I talk to? ###

If you have any questions or wish to contribute to the project, please contact the owner of this project at msoneal0(at)gmail.com.