#ifndef AUD_FILE_H
#define AUD_FILE_H

//    This file is part of JustAudio.

//    JustAudio is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    JustAudio is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with BashWire under the GPL.txt file. If not, see
//    <http://www.gnu.org/licenses/>.

#include <QFile>
#include <QObject>
#include <QString>
#include <QTimer>
#include <QStringList>
#include <QMediaPlayer>

class AudFile : public QFile
{
    Q_OBJECT

private:

    QTimer *timer;
    qint64  bytesRead;
    qint64  offset;
    bool    activeInterupt;
    int     v1TagSize;

public:

    AudFile(QObject *parent = 0);

    bool   openFile(const QString &path);
    bool   seek(qint64 off);
    qint64 getOffset();
    qint64 audSize();

    ~AudFile();

public slots:

    void userInterupt();
    void userResume();

signals:

    void bytePos(qint64 off);
    void endOfPlayback();
};

#endif // AUD_FILE_H
